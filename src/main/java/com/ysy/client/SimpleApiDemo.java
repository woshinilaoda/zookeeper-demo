package com.ysy.client;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.List;

/**
 * @program: zookeeper-demo
 * @description: 简单api demo
 * @author: yeshiyuan
 * @create: 2019-06-06 16:34
 **/
public class SimpleApiDemo {

    public static void main(String[] args) throws IOException, KeeperException, InterruptedException {
        ZooKeeper zk = new ZooKeeper("192.168.5.35:2181", 30000, new Watcher() {
            public void process(WatchedEvent event) {
                System.out.println("事件类型为：" + event.getType());
                System.out.println("事件发生的路径：" + event.getPath());
                System.out.println("通知状态为：" + event.getState());
                System.out.println("");
            }
        });

        // 创建一个目录节点, 数据是"节点数据",不进行ACL权限控制，节点为永久性的(即客户端shutdown了也不会消失)
       // zk.create("/test9", "节点数据".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        //test9节点创建子数据(注册第一个watch)
        Stat stat = zk.exists("/test9/aaa", true);
        if (stat == null) {
            zk.create("/test9/aaa", "children".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
        } else {
            System.out.println(stat.toString());
        }

        //取得/test9节点下的子节点名称,返回List<String>
        List<String> childData = zk.getChildren("/test9", true);
        //test9节点创建子数据(当子节点数据创建时，getChildren的watch触发)
        zk.create("/test9/111", "aaaa".getBytes(),ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        //test9节点创建子数据(当子节点数据创建时，getChildren的watch触发)
        zk.getChildren("/test9", true);
        zk.create("/test9/222", "aaaa".getBytes(),ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        //取得/test9节点下的数据,返回byte[]（当setData时，getData的watch触发）
        byte[] dataBytes = zk.getData("/test9", true, null);
        System.out.println("数据：" + new String(dataBytes));
        //修改节点数据，第三个参数为版本，如果是-1，那会无视被修改的数据版本，直接改掉
        zk.setData("/test9", "打工没用".getBytes(), -1);
       //重新设置watch，zookeeper中watch被调用之后需要重新设置
        zk.exists("/test9/aaa", true);
        //删除/root/childone这个节点，第二个参数为版本，－1的话直接删除，无视版本（删除触发上面的exist的watch）
        zk.delete("/test9/aaa", -1);
        //关闭session
        zk.close();
    }

}
